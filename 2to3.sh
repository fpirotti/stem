grep -rl "from PyQt4.QtGui import \*" ./ | sed -i  's/from PyQt4.QtGui import \*/from qgis.PyQt.QtGui import */g' 
grep -rl "from PyQt4.QtCore import \*" ./ | sed -i  's/from PyQt4.QtCore import \*/from qgis.PyQt.QtCore import */g' 


grep -rl "from PyQt4.QtGui import \*" ./ | xargs sed -i  "s/from PyQt4.QtGui import \*/from qgis.PyQt.QtGui import */g" 
grep -rl "from PyQt4.QtCore import \*" ./ | xargs sed -i  "s/from PyQt4.QtCore import \*/from qgis.PyQt.QtCore import */g" 

grep -rl "raise IOError('" ./ | xargs sed -i  "s/raise '\(.*\)'/raise IOError('\1'))/g"

qgis2to3 -w ./ 2>err.txt 